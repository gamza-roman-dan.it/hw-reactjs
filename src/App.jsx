import Modal from "./components/modal/index.jsx";
import Header from "./components/header/index.jsx";
import Router from "./appRouter.jsx"
import {fetchAllProducts} from "./components/reduce/fetchProductSlice.js";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";

function App() {

  const dispatch = useDispatch();


  useEffect(() => {
    dispatch(fetchAllProducts());
  }, []);


  const isModal = useSelector( state => state.modal.isModal);
  const {photo, name, price, color, article} = useSelector( state => state.modal.objectForModal);
  const basked = useSelector( state => state.productContainer.forBasked);
  const selected = useSelector( state => state.productContainer.forStar);

  return (
    <>
      <Header basked={basked.length} selected={selected.length}/>
        <Router/>

      {isModal ?
          <Modal photo={photo}
                 title={name}
                 txt={price}
                 color = {color}
                 id = {article}
                 />
          : null}
    </>
  )
}

export default App
