import style from "./baskedAndStarContainer.module.scss"
import {useLocation} from "react-router-dom";
import PropTypes from "prop-types";
import Card from "../card/index.jsx";
import {useSelector} from "react-redux";
import FormForBuy from "../form/index.jsx";
const BaskedAndStarContainer = () => {

    const location = useLocation();
    const product = useSelector( state => state.product.data);


    const basket = location.pathname === "/basket";
    if(basket) {
        const basketArr = useSelector(state => state.productContainer.forBasked);

        return (
            <>
                <section className={style.container}>
                    {basketArr.length > 0 ? basketArr.map(({name, price, photo, article})=>  <Card photo={photo}
                                                                                                   title={name}
                                                                                                   sale={price}
                                                                                                   key={article}
                                                                                                   id={article}
                                                                                                   product = {product}
                                                                                                   baskedButton ={true}
                    />) :"у вас ще немає товарів у кошику"}
                </section>
                <section className={style.form}>
                    {basketArr.length > 0 ? <FormForBuy/> : "додайте товар й зявиться форма"}
                </section>
            </>
        )
    }


    const star = location.pathname === "/star";
    if(star){
        const starArr = useSelector(state => state.productContainer.forStar);

        return (
            <section className={style.container}>
                {starArr.length > 0 ? starArr.map(({name, price, photo, article})=>  <Card photo={photo}
                                                                                               title={name}
                                                                                               sale={price}
                                                                                               key={article}
                                                                                               id={article}
                                                                                               product = {product}
                                                                                               starButton = {true}
                />) : "у вас ще немає обраних товарів"}
            </section>
        )
    }
}

BaskedAndStarContainer.prototypes = {
    modal: PropTypes.bool.isRequired,
    article: PropTypes.string.isRequired,
    setProduct: PropTypes.func.isRequired,
    selectedSet: PropTypes.func.isRequired,
};

export default BaskedAndStarContainer