import {describe, expect, test} from '@jest/globals';

import Modal from "./index.jsx";
import {fireEvent, render, screen} from "@testing-library/react";
import {Provider, useDispatch} from "react-redux";
import {changeModal} from "../reduce/modalSlice.js";
import {store} from "../../store.js";

const Wrapper = () => {

    const dispatch = useDispatch();

    return(
        <>
            <button onClick={() => {dispatch(changeModal())}} >open</button>
            <Modal id={"1111"} title={"hello from modal"}></Modal>
        </>
    )
}

describe('button snapshot test', () => {
    test("should button render", () => {
        render(
            <Provider store={store}>
                <Wrapper/>
            </Provider>);

        const openButton = screen.getByText("open")

        fireEvent.click(openButton);

        expect(screen.getByTestId("modal")).toBeInTheDocument();

    })
});
