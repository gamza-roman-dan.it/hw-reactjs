import PropTypes from "prop-types";
import style from "./modal.module.scss"
import {changeModal} from "../reduce/modalSlice.js";
import {useDispatch, useSelector} from "react-redux";
import {addBasked, removeBasked} from "../reduce/productContainerSlice.js";
import Button from "../button/index.jsx";

const Modal = ({photo, title, txt, color, id}) => {

    const dispatch = useDispatch();
    const modalObject = useSelector(state => state.modal.objectForModal);

    const basked = useSelector( state => state.productContainer.forBasked);
    const findBasked = basked.find(el => el.article === id);


    return(
        <>
            <div className={style.modal} data-testid = "modal">
                <div className={style.contentModal}>
                    {photo ? <img src={photo} alt="foto" className={style.img}/> : null}
                    <h1>{title}</h1>
                    <p>колір - {color}</p>
                    <p>ціна - {txt}</p>
                    <Button onClick={() => {if(findBasked){
                        dispatch(removeBasked(id)); dispatch(changeModal());
                    }else {
                        dispatch(addBasked(modalObject)); dispatch(changeModal());
                    }}}>
                        {findBasked ? "remove this card ?" : "add this card ?"}
                    </Button>
                    <button className={style.x} onClick={() => {dispatch(changeModal())}}>x</button>
                </div>
                <div className={style.background} onClick={() => {dispatch(changeModal())}}></div>
            </div>
        </>
    )
};

Modal.prototypes = {
    photo: PropTypes.string,
    title: PropTypes.string,
    txt: PropTypes.string,
    color: PropTypes.string,
    id: PropTypes.string.isRequired,
};

export default Modal