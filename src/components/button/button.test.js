import {describe, expect, test} from '@jest/globals';

import Button from "./index.jsx";
import { render } from "@testing-library/react";

describe('button snapshot test', () => {
    test("should button render", () => {
        const {asFragment} = render(<Button> hey from button </Button>);

        expect(asFragment()).toMatchSnapshot();
    })
});

