import PropTypes from "prop-types";
import style from "./button.module.scss"

const Button = ({type, onClick, children, css }) => {
    return(
        <>
            <button type={type} className={ css ? css : style.button} onClick={onClick}>{children}</button>
        </>
    )
};
Button.prototypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
};

export default Button;