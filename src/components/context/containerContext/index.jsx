import {createContext} from "react";

const ContainerContext = createContext(null);

export default ContainerContext;