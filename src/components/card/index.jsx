import PropTypes from "prop-types";
import style from "./card.module.scss"
import Button from "../button/index.jsx";
import Star from "../svg/star/index.jsx";
import {useDispatch, useSelector} from "react-redux";
import {changeModal, modalObject} from "../reduce/modalSlice.js"
import {addStar, removeBasked, removeStar} from "../reduce/productContainerSlice.js";
import {useContext} from "react";
import ContainerContext from "../context/containerContext/index.jsx";
const Card = ({photo = "", title = "", sale = "", id, product, baskedButton = false , starButton = false}) => {

    const dispatch = useDispatch();
    let post = {};

    const selected = useSelector( state => state.productContainer.forStar);
    const findSelected = selected.find(el => el.article === id);

    const basked = useSelector( state => state.productContainer.forBasked);
    const findBasked = basked.find(el => el.article === id);


    const findModal = (id, product) => { post = product.find(article => id === article.article); dispatch(modalObject(post))};

    const {reversContainer} = useContext(ContainerContext);


    return (

            <div className={starButton || baskedButton ? style.container : !reversContainer ? style.secondBox : style.container}>
                <img src={photo} alt="foto" className={style.image}/>
                <div>
                    <h1>{title}</h1>
                    <p>ціна: {sale} грн</p>
                    <div className={style.box}>
                         <Button
                             css={starButton ? style.non : false}
                             onClick={() => {baskedButton ? dispatch(removeBasked(id)) : dispatch(changeModal()); findModal(id, product) }}>
                            {baskedButton ? "x" : findBasked ? "remove this card ?" : "add this card ?"}
                         </Button>
                        <div className={`${style.star} ${findSelected ? style.active : ""}`}
                             onClick={() => {
                                 if(findSelected){
                                     dispatch(removeStar(id));
                                 }else {
                                 findModal(id, product); dispatch(addStar(post))
                                  }}}>
                            <Star fill={'darkslateblue'}/>
                        </div>
                    </div>
                </div>
            </div>

    );
};

Card.prototypes = {
    photo: PropTypes.string,
    title: PropTypes.string,
    sale: PropTypes.string,
    id: PropTypes.string,
    product: PropTypes.array,
    baskedButton: PropTypes.bool,
    starButton: PropTypes.bool,
};

export default Card