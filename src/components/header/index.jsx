import style from "./header.module.scss"
import Basket from "../svg/basket/index.jsx";
import Star from "../svg/star/index.jsx";
import PropTypes from "prop-types";
import {NavLink} from "react-router-dom";


const Header = ({selected, basked}) => {


    return(
        <header className={style.header}>
            <div className={style.svgBox}>
                <NavLink end to="/" className={({isActive}) => isActive ? style.active : ""} >HOME</NavLink>
            </div>
            <div className={style.svgBox}>
                <NavLink end to="/basket" className={({isActive}) => isActive ? style.active : ""} ><Basket/></NavLink>
                <p>{basked}</p>
            </div>
            <div className={style.svgBox}>
                <NavLink end to="/star" className={({isActive}) => isActive ? style.active : ""} ><Star/></NavLink>
                <p>{selected}</p>
            </div>
        </header>
    )
};

Header.prototypes = {
    selected: PropTypes.number,
    basked: PropTypes.number,
};

export default Header