import style from "./form.module.scss"
import CustomInput from "../customInput/index.jsx";
import {Formik, Form} from "formik";
import * as Yup from 'yup';
import {useSelector, useDispatch} from "react-redux";
import {removeAllBasked} from "../reduce/productContainerSlice.js";


const validationSchema = Yup.object().shape({
    firstName: Yup.string()
        .min(2, 'Too Short!')
        .max(15, 'Too Long!')
        .required('Required'),
    lastName: Yup.string()
        .min(2, 'Too Short!')
        .max(15, 'Too Long!')
        .required('Required'),
    age: Yup.number()
        .min(2, 'too small')
        .max(100, 'Too large!')
        .required('Required'),
    address: Yup.string()
        .required('Required'),
    phone: Yup.number()
        .required('Required'),
});
const FormForBuy = () => {

    const dispatch = useDispatch();
    const basked = useSelector( state => state.productContainer.forBasked);


    return (
        <Formik
            initialValues={{
                firstName: "",
                lastName: "",
                age: "",
                address: "",
                phone: "",
            }}

            validationSchema = {validationSchema}

            onSubmit={(values) => {
                console.log(values)
                console.log(basked)
                dispatch(removeAllBasked())
            }}>
            {({isValid}) => {
                return(
                    <Form>

                        <h1 className={style.title}> щоб придбати товар заповніть форму </h1>

                        <CustomInput
                            name = "firstName"
                            type="text"
                            placeholder= "first name"
                        />
                        <CustomInput
                            name = "lastName"
                            type="text"
                            placeholder= "last name"
                        />
                        <CustomInput
                            name = "age"
                            type="text"
                            placeholder= "age"
                        />
                        <CustomInput
                            name = "address"
                            type="text"
                            placeholder= "address"
                        />
                        <CustomInput
                            name = "phone"
                            type="text"
                            placeholder= "phone"
                        />
                        <button disabled={!isValid}
                                className={style.button}
                                type={"submit"}
                                // onClick={() => {}}
                        > купити </button>
                    </Form>
                )
            }}
        </Formik>
    );
};

export default FormForBuy