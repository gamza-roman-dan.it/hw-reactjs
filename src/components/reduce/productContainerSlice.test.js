import {describe, expect, test} from '@jest/globals';

import productContainerSlice from "./productContainerSlice.js";


//fo basked-------------------------------------------------------------------------------------

describe("basked array changes", () => {

    test("should be add object", () => {
        const state = {forBasked: []};
        const action = {type: "productContainer/addBasked", payload: {}};

        expect(productContainerSlice(state, action)).toEqual({forBasked: [{}]});
    })

    test("should be remove object", () => {
        const state = {forBasked: [{article: "123"}]};
        const action = {type: "productContainer/removeBasked", payload: "123"};

        expect(productContainerSlice(state, action)).toEqual({forBasked: []});
    })

    test("should be remove all object", () => {
        const state = {forBasked: [{},{},{}]};
        const action = {type: "productContainer/removeAllBasked"};

        expect(productContainerSlice(state, action)).toEqual({forBasked: []});
    })
});


//fo star-------------------------------------------------------------------------------------


describe("star array changes", () => {

    test("should be add star object", () => {
        const state = {forStar: []};
        const action = {type: "productContainer/addStar", payload: {}};

        expect(productContainerSlice(state, action)).toEqual({forStar: [{}]});
    })

    test("should be remove star object", () => {
        const state = {forStar: [{article: "123"}]};
        const action = {type: "productContainer/removeStar", payload: "123"};

        expect(productContainerSlice(state, action)).toEqual({forStar: []});
    })
});


