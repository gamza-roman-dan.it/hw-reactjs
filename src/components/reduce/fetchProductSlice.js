import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import {json} from "react-router-dom";

export const fetchAllProducts = createAsyncThunk(
    'product/fetchAllProducts',
    async () => {
        const {data} = await axios.get("./products.json");
        return data;
    },
);

const productSlice = createSlice({
    name: "product",
    initialState: {
        data: [],
        isLoading: true,
    },
    reducers: {

    },
    extraReducers: (builder) => {
        builder.addCase(fetchAllProducts.fulfilled, (state, action) => {
            state.data = action.payload;
            state.isLoading = false;
        });
    },
});

export const {  } = productSlice.actions;
export default productSlice.reducer;