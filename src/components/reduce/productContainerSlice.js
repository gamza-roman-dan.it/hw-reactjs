import { createSlice } from "@reduxjs/toolkit";

const selectedLS = JSON.parse(localStorage.getItem("selected"));
const baskedLS = JSON.parse(localStorage.getItem("basked"));

const initialState = {
    forBasked: baskedLS ? baskedLS : [],
    forStar: selectedLS ? selectedLS : [],
}


const productContainerSlice = createSlice({
    name: "productContainer",
    initialState,
    reducers: {
        addBasked: (state, action) => {
            state.forBasked.push(action.payload);
            localStorage.setItem("basked", JSON.stringify(state.forBasked));
        },
        addStar: (state, action) => {
            state.forStar.push(action.payload);
            localStorage.setItem("selected", JSON.stringify(state.forStar))
        },
        removeStar: (state, action) => {
            state.forStar = state.forStar.filter(el => el.article !== action.payload);
            localStorage.setItem("selected", JSON.stringify(state.forStar));
        },
        removeBasked: (state, action) => {
            state.forBasked = state.forBasked.filter(el => el.article !== action.payload);
            localStorage.setItem("basked", JSON.stringify(state.forBasked));
        },
        removeAllBasked: (state) => {
            state.forBasked = [];
            localStorage.setItem("basked", JSON.stringify(state.forBasked));
        },

    },

});

export const { addBasked, addStar,removeStar, removeBasked, removeAllBasked } = productContainerSlice.actions;
export default productContainerSlice.reducer;