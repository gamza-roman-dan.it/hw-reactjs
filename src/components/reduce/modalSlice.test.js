import {describe, expect, test} from '@jest/globals';

import ModalSlice from "./modalSlice.js";

describe('Modal Reducer works', () => {
    test("should be modal reducer open modal", () => {
        const state = { isModal: false };
        const action = { type: "modal/changeModal"}

        expect(ModalSlice(state, action)).toEqual({isModal: true})
    })
    test("should be modal reducer close modal", () => {
        const state = { isModal: true};
        const action = { type: "modal/changeModal"}

        expect(ModalSlice(state, action)).toEqual({isModal: false})
    })
});
