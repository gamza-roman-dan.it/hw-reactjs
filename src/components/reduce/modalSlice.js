import { createSlice } from "@reduxjs/toolkit";

const modalSlice = createSlice({
    name: "modal",
    initialState: {
        isModal: false,
        objectForModal: {},
    },
    reducers: {
        changeModal: (state) => {
            state.isModal = !state.isModal;
        },
        modalObject: (state, action) => {
            state.objectForModal = action.payload;
        }
    },

});

export const { changeModal, modalObject } = modalSlice.actions;
export default modalSlice.reducer;
