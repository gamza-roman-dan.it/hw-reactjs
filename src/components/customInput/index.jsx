import style from "./customInput.module.scss"
import {useField} from "formik";

const CustomInput = (props) => {

    const [field, meta] = useField(props);


    return(
        <>
            <input
                className={style.input}
                {...field}
                {...props}
            />
            {meta.touched && meta.error && <p style={{color:  "red"}}> {meta.error} </p>}
        </>
    )
}

export default CustomInput