import Card from "../card/index.jsx";
import style from "./cardContainer.module.scss"
import {useSelector} from "react-redux";
import Button from "../button/index.jsx";
import {useContext} from "react";
import ContainerContext from "../context/containerContext/index.jsx";

const CardContainer = () => {

    const product = useSelector( state => state.product.data);
    const isLoading = useSelector( state => state.product.isLoading);

    const {reversContainer, setReversContainer} = useContext(ContainerContext);


    return(
        <>
            <Button onClick={() => {setReversContainer(prev => !prev)}} css={style.button} >change container</Button>

            <section className={reversContainer ? style.cardContainer : style.twoCardContainer}>
                {isLoading && <span className={style.loading}>loading...</span>}
                {product && product.map(({name, price, photo, article}) => <Card photo={photo}
                                                                                  title={name}
                                                                                  sale={price}
                                                                                  key={article}
                                                                                  id={article}
                                                                                  product = {product}
                />)}
            </section>

        </>
    )
}

export default CardContainer