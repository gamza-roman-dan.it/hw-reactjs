import { configureStore } from '@reduxjs/toolkit'
import productSlice from './components/reduce/fetchProductSlice.js'
import modalSlice from "./components/reduce/modalSlice.js";
import productContainer from "./components/reduce/productContainerSlice.js"

export const store = configureStore({
    reducer: {
        product: productSlice,
        modal: modalSlice,
        productContainer,
    },
})

