import {Routes, Route} from "react-router-dom";
import CardContainer from "./components/cardContainer/index.jsx";
import BaskedAndStarContainer from "./components/baskedAndStarContainer/index.jsx";
import ContainerContext from "./components/context/containerContext/index.jsx";
import {useState} from "react";

const Router = () => {

    const [reversContainer, setReversContainer] = useState(false);

    const value = {
        reversContainer,
        setReversContainer,
    };

    return(
        <ContainerContext.Provider value = {value}>

            <Routes>
                <Route path="/" element={ <CardContainer/>}/>
                <Route path="/basket" element={<BaskedAndStarContainer/>}/>
                <Route path="/star" element={<BaskedAndStarContainer/>}/>
            </Routes>
        </ContainerContext.Provider>

    )
};

export default Router