import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import { ErrorBoundary } from "react-error-boundary";
import {BrowserRouter} from "react-router-dom";
import { store } from './store.js'
import { Provider } from 'react-redux'


ReactDOM.createRoot(document.getElementById('root')).render(
    <ErrorBoundary fallback={<div>Something went wrong</div>}>
        <BrowserRouter>
            <Provider store={store}>
                <App />
            </Provider>
        </BrowserRouter>
    </ErrorBoundary>
)
